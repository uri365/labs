import os
import pyodbc as dbapi

connstr = 'DRIVER={SQL Server}; SERVER=.; DATABASE=air; Trusted_Connection=yes'


def connect():
    conn = dbapi.connect(connstr)
    return conn


def create_table(conn):
    cur = conn.cursor()
    cur.execute('''
            CREATE TABLE Flights(
                airline VARCHAR(20),
                flight_num INTEGER,
                source VARCHAR(4),
                destination VARCHAR(4)
            )''')
    conn.commit()


def insert_data(conn):
    cur = conn.cursor()
    flights = [
        ('ElAl', 1, 'TLV', 'JFK'),
        ('Malaysia', 370, 'KUL', 'PEK'),
        ('Oceanic', 815, 'SYD', 'LAX'),
        ('ElAl', 5, 'TLV', 'LAX')
    ]

    for flight in flights:
        cur.execute(
            '''INSERT INTO Flights(airline, flight_num, source, destination)
               VALUES(?,?,?,?)''',
            (flight[0], flight[1], flight[2], flight[3])
        )
        conn.commit()


def find_lax(conn):
    cur = conn.cursor()
    cur.execute('''
        SELECT * FROM Flights
        WHERE destination = ?
        ''', ('LAX',)
    )
    results = cur.fetchall()
    for row in results:
        print '- airline={0}, flight={1}, from={2}, to={3}'.format(
            row[0], row[1], row[2], row[3]
        )



if __name__ == '__main__':
    conn = connect()
    create_table(conn)
    insert_data(conn)
    find_lax(conn)
    conn.close()



