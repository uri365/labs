import webapp2 as webapp
import json

class MainHandler(webapp.RequestHandler):
    def get(self):
        self.response.write('Hello webapp2!')

class RootHandler(webapp.RequestHandler):
    def get(self):
        self.redirect('/main')

class ApiHandler(webapp.RequestHandler):
    def get(self, id):
        self.response.headers['Content-Type'] = 'application/javascript'
        reply_obj = {
            'id': id,
            'first_name': 'David',
            'last_name': 'Ben Ishai',
            'occupation': 'King',
            'age': 70
            }
        self.response.write(json.dumps(reply_obj))

        
application = webapp.WSGIApplication([
    ('/', RootHandler),
    ('/main', MainHandler),
    ('/api/(.*)', ApiHandler),
], debug=True)


if __name__ == '__main__':
    import wsgiref.simple_server
    http_srv = wsgiref.simple_server.make_server('localhost', 8080, application)
    http_srv.serve_forever()
