
def application(environ, start_response):
    status = '200 OK'
    headers = [
        ('Context-Type', 'text/html'),
    ]
    start_response(status, headers)
    yield '<html><head></head><body><h1>The environ variable</h1>'

    yield '<table>'
    for key, value in sorted(environ.items()):
        yield '<tr><td>{key}</td><td>{val}</td></tr>'.format(key=key, val=value)
    yield '</table>'

    yield '</body></html>'



if __name__ == '__main__':
    import wsgiref.simple_server
    http_srv = wsgiref.simple_server.make_server('localhost', 8080, application)
    http_srv.serve_forever()

