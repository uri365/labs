
def application(environ, start_response):
    status = '200 OK'
    headers = [
        ('Context-Type', 'text/html'),
    ]
    start_response(status, headers)
    yield '<html><head></head><body><h1>Hello WSGI</h1></body></html>'


if __name__ == '__main__':
    import wsgiref.simple_server
    http_srv = wsgiref.simple_server.make_server('localhost', 8080, application)
    http_srv.serve_forever()

